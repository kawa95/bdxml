<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : input.xsl
    Created on : 30 octobre 2015, 13:20
    Author     : Max_2
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>input.xsl</title>
            </head>
            <body>
                
            <h2>Ma liste de séries :</h2>
            
            <table border="1">
              <tr bgcolor="#9acd32">
                <th>Series</th>
              </tr>
              <xsl:for-each select="/series/serie">
              <tr>
                <td><xsl:value-of select="."/></td>
              </tr>
              </xsl:for-each>
            </table>
                        
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
